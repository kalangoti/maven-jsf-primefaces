package br.com.anderson.maven_jsf_primefaces.controller;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.com.anderson.maven_jsf_primefaces.dao.EmailDAO;
import br.com.anderson.maven_jsf_primefaces.model.EmailModel;

@ManagedBean(name = "enviarMB")
@ViewScoped
public class EnviarManagedBean {

	private EmailDAO emailDAO = new EmailDAO();
	private EmailModel emailModel = new EmailModel();
	private String destinatarios;

	@PostConstruct
	public void init() {
	}

	public String enviarEmail() {
		if (emailModel == null) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Entidade nula, favor preencher as informações corretamente.", ""));
			return null;
		}

		if (destinatarios.contains(";")) {
			String[] split = destinatarios.split(";");

			for (int i = 0; i < split.length; i++) {
				EmailModel novoEmailModel = new EmailModel();
				novoEmailModel.setAssunto(emailModel.getAssunto());
				novoEmailModel.setConteudoMensagem(emailModel.getConteudoMensagem());
				novoEmailModel.setDestinatario(split[i].trim());

				if (emailDAO.inserirEmail(novoEmailModel)) {
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_INFO, "E-mail enviado com sucesso!", ""));
				} else {
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao tentar enviar o e-mail.", ""));
				}
			}

		} else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Os campos do e-amil não foram preenchidos corretamente, por favor realize o seu preenchimento.",
					""));
		}

		return null;
	}

	/**
	 * @return the emailModel
	 */
	public EmailModel getEmailModel() {
		return emailModel;
	}

	/**
	 * @param emailModel the emailModel to set
	 */
	public void setEmailModel(EmailModel emailModel) {
		this.emailModel = emailModel;
	}

	/**
	 * @return the destinatarios
	 */
	public String getDestinatarios() {
		return destinatarios;
	}

	/**
	 * @param destinatarios the destinatarios to set
	 */
	public void setDestinatarios(String destinatarios) {
		this.destinatarios = destinatarios;
	}
}
