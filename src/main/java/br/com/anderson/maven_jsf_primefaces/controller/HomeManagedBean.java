package br.com.anderson.maven_jsf_primefaces.controller;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "homeMB")
@ViewScoped
public class HomeManagedBean {

	@PostConstruct
	public void init() {
	}
}
