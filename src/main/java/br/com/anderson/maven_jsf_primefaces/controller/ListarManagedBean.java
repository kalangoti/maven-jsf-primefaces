package br.com.anderson.maven_jsf_primefaces.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import br.com.anderson.maven_jsf_primefaces.dao.EmailDAO;
import br.com.anderson.maven_jsf_primefaces.model.EmailModel;

@ManagedBean(name = "listarMB")
@ViewScoped
public class ListarManagedBean {
	
	private EmailDAO emailDAO = new EmailDAO();
	private EmailModel emailModel = new EmailModel();
	private String textoBusca;
	private List<EmailModel> lista = new ArrayList<EmailModel>();

	@PostConstruct
	public void init() {
		lista.clear();
	}
	
	public String buscarEmailPorAssunto() {
		if (textoBusca == null || textoBusca.isEmpty()) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Entidade nula, favor preencher as informações corretamente.", ""));
			return null;
		}

		lista.clear();
		lista = emailDAO.buscarPorAssunto(getTextoBusca());
		
		if (lista.isEmpty()) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_WARN, "Não existir nenhum e-mail com o assunto informado", ""));
		}

		return null;
	}
	
	public String buscarEmailPorDestinatario() {
		if (textoBusca == null || textoBusca.isEmpty()) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Entidade nula, favor preencher as informações corretamente.", ""));
			return null;
		}

		lista.clear();
		lista = emailDAO.buscarPorDestinatario(getTextoBusca());
		
		if (lista.isEmpty()) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_WARN, "Não existir nenhum e-mail com o destinatário informado", ""));
		}

		return null;
	}

	/**
	 * @return the emailModel
	 */
	public EmailModel getEmailModel() {
		return emailModel;
	}

	/**
	 * @param emailModel the emailModel to set
	 */
	public void setEmailModel(EmailModel emailModel) {
		this.emailModel = emailModel;
	}

	/**
	 * @return the textoBusca
	 */
	public String getTextoBusca() {
		return textoBusca;
	}

	/**
	 * @param textoBusca the textoBusca to set
	 */
	public void setTextoBusca(String textoBusca) {
		this.textoBusca = textoBusca;
	}

	/**
	 * @return the lista
	 */
	public List<EmailModel> getLista() {
		return lista;
	}

	/**
	 * @param lista the lista to set
	 */
	public void setLista(List<EmailModel> lista) {
		this.lista = lista;
	}
}
