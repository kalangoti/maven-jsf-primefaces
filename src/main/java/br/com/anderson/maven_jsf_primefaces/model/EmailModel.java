package br.com.anderson.maven_jsf_primefaces.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity (name = "email")
public class EmailModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", nullable=false, unique=true)
    private int id;
	
	@Column(name="assunto", nullable=false)
	private String assunto;
	
	@Column(name="destinatario", nullable=false)
	private String destinatario;
	
	@Column(name="conteudoMensagem", nullable=false)
	private String conteudoMensagem;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the assunto
	 */
	public String getAssunto() {
		return assunto;
	}

	/**
	 * @param assunto the assunto to set
	 */
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	/**
	 * @return the destinatario
	 */
	public String getDestinatario() {
		return destinatario;
	}

	/**
	 * @param destinatario the destinatario to set
	 */
	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	/**
	 * @return the conteudoMensagem
	 */
	public String getConteudoMensagem() {
		return conteudoMensagem;
	}

	/**
	 * @param conteudoMensagem the conteudoMensagem to set
	 */
	public void setConteudoMensagem(String conteudoMensagem) {
		this.conteudoMensagem = conteudoMensagem;
	}
}
