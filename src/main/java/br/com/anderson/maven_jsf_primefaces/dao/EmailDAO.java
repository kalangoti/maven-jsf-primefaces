package br.com.anderson.maven_jsf_primefaces.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.anderson.maven_jsf_primefaces.model.EmailModel;

public class EmailDAO {

	private EntityManagerFactory factory = Persistence.createEntityManagerFactory("teste");

	private EntityManager em = factory.createEntityManager();

	public boolean inserirEmail(EmailModel objModel) {
		try {
			em.getTransaction().begin();
			em.merge(objModel);
			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
			return false;
		}
	}

	public List<EmailModel> buscarPorAssunto(String texto) {
		try {
			em.getTransaction().begin();
			@SuppressWarnings("unchecked")
			List<EmailModel> emails = em.createQuery("from email where assunto = '" + texto + "'").getResultList();
			em.getTransaction().commit();
			return emails;
		} catch (Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
			return null;
		}
	}

	public List<EmailModel> buscarPorDestinatario(String texto) {
		try {
			em.getTransaction().begin();
			@SuppressWarnings("unchecked")
			List<EmailModel> emails = em.createQuery("from email where destinatario like '%" + texto + "%'").getResultList();
			em.getTransaction().commit();
			return emails;
		} catch (Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
			return null;
		}
	}
}
