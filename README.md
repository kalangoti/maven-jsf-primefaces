# Projeto: maven-jsf-primefaces


## Autor: Anderson Santo


## Tecnologias usadas:

- Tomcat 8.5.43 **

- Java 8

- Eclipse 2019-03 (4.11.0)

- Primefaces 7.0

- Hibernate 5.4.4.Final

- Apache Derby 10.12.1.1

- JSF 2.2

- JPA 2.1


** Foi escolhido essa versão porque no site do Apache Tomcat tem uma nota dizendo que:

"Os usuários do Tomcat 8.0.x devem estar cientes de que ele atingiu o fim da vida útil. Os usuários do Tomcat 8.0.x devem atualizar para a versão 8.5.x ou posterior.

Nota: O fim da vida útil foi anunciado apenas para 8.0.x. 8.5.x não é afetado por este anúncio."


## Neste sistema, o usuário terá as seguintes funcionalidades:
1. Enviar e-mail: O usuário informará o e-mail dos destinatários (não tem um
limite máximo definido para a quantidade de destinatários), o assunto e o
conteúdo da mensagem que será enviada no e-mail. Ao clicar no botão
enviar, o e-mail deverá ser persistido na base de dados. Se a operação for
realizada com sucesso ou erro, uma mensagem deverá ser apresentada ao
usuário, comunicando o ocorrido.

2. Listar e-mails baseado no assunto: O usuário informará o assunto da
mensagem e clicará no botão listar, a aplicação buscará na base de dados
todos os e-mails cujo o assunto da mensagem é igual ao que o usuário
informou. Após buscar as informações, a aplicação deverá apresentar o
resultado ao usuário e, se não existir nenhum e-mail com o assunto
informado, uma mensagem comunicando o fato deverá ser apresentada ao
usuário.

3. Listar e-mails enviados para um determinado destinatário: O usuário
informará o e-mail do destinatário e clicará no botão listar. A aplicação
buscará na base de dados todos os e-mails cujo o destinatário contém o email informado pelo usuário. Após buscar as informações, a aplicação
deverá apresentar o resultado ao usuário e, se nenhum e-mail for encontrado,
uma mensagem comunicando o fato deverá ser apresentada ao usuário. 